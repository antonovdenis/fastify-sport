import fastifyEnv, { FastifyEnvOptions } from "@fastify/env";
import fp from "fastify-plugin";

export default fp<FastifyEnvOptions>(
  async function envConfig(fastify, opts) {
    await fastify.register(fastifyEnv, {
      confKey: "secrets",
      data: opts.data,
      schema: {
        type: "object",
        required: ["SPORT_API_KEY"],
        properties: {
          SPORT_API_KEY: {
            type: "string",
          },
        },
      },
    });
  },
  { name: "env-config" }
);

declare module "fastify" {
  export interface FastifyInstance {
    secrets: {
      SPORT_API_KEY: string;
    };
  }
}
