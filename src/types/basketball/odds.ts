import { BasketballGame } from "./games";

interface LeagueInfo {
  id: number;
  name: string;
  type: string;
  season: string;
  logo: string;
}

interface CountryInfo {
  id: number;
  name: string;
  code: string;
  flag: string;
}

interface BetValue {
  value: string;
  odd: string;
}

interface Bet {
  id: number;
  name: string;
  values: BetValue[];
}

interface Bookmaker {
  id: number;
  name: string;
  bets: Bet[];
}

export interface ResponseItem {
  league: LeagueInfo;
  country: CountryInfo;
  game: BasketballGame;
  bookmakers: Bookmaker[];
}
