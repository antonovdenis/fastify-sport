interface GameStatus {
  long: string;
  short: string;
  timer: null;
}

interface LeagueInfo {
  id: number;
  name: string;
  type: string;
  season: string;
  logo: string;
}

interface CountryInfo {
  id: number;
  name: string;
  code: string;
  flag: string;
}

interface TeamInfo {
  id: number;
  name: string;
  logo: string;
}

interface ScoreDetails {
  quarter_1: number;
  quarter_2: number;
  quarter_3: number;
  quarter_4: number;
  over_time: null;
  total: number;
}

export interface BasketballGame {
  id: number;
  date: string;
  time: string;
  timestamp: number;
  timezone: string;
  stage: null;
  week: null;
  status: GameStatus;
  league: LeagueInfo;
  country: CountryInfo;
  teams: {
    home: TeamInfo;
    away: TeamInfo;
  };
  scores: {
    home: ScoreDetails;
    away: ScoreDetails;
  };
}
