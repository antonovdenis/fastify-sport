interface League {
  id: number;
  name: string;
  country: string;
  logo: string;
  flag: string;
  season: number;
}

interface OddFixture {
  id: number;
  timezone: string;
  date: string;
  timestamp: number;
}

interface BetValue {
  value: string;
  odd: string;
}

interface Bet {
  id: number;
  name: string;
  values: BetValue[];
}

interface Bookmaker {
  id: number;
  name: string;
  bets: Bet[];
}

export interface ResponseOdds {
  league: League;
  fixture: OddFixture;
  update: string;
  bookmakers: Bookmaker[];
}
