export interface VolleyballMatch {
  id: number;
  date: string;
  time: string;
  timestamp: number;
  timezone: string;
  week: string;
  status: Status;
  country: Country;
  league: League;
  teams: Teams;
  scores: Scores;
  periods: Periods;
}

interface Status {
  long: string;
  short: string;
}

interface Country {
  id: number;
  name: string;
  code: string;
  flag: string;
}

interface League {
  id: number;
  name: string;
  type: string;
  logo: string;
  season: number;
}

interface Teams {
  home: TeamDetail;
  away: TeamDetail;
}

interface TeamDetail {
  id: number;
  name: string;
  logo: string;
}

interface Scores {
  home: number;
  away: number;
}

interface Periods {
  first: SetScore;
  second: SetScore;
  third: SetScore;
  fourth?: SetScore;
  fifth?: SetScore;
}

interface SetScore {
  home: number | null;
  away: number | null;
}
