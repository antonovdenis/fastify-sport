export interface GameBettingInfo {
  game: Game;
  league: League;
  country: Country;
  update: string;
  bookmakers: Bookmaker[];
}

interface Game {
  id: number;
}

interface League {
  id: number;
  name: string;
  type: string;
  season: number;
  logo: string;
}

interface Country {
  id: number;
  name: string;
  code: string;
  flag: string;
}

interface Bookmaker {
  id: number;
  name: string;
  bets: Bet[];
}

interface Bet {
  id: number;
  name: string;
  values: BetValue[];
}

interface BetValue {
  value: string;
  odd: string;
}
