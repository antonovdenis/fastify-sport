export interface NFLGameDetails {
  game: Game;
  league: League;
  teams: Teams;
  scores: Scores;
}

interface Game {
  id: number;
  stage: string;
  week: string;
  date: GameDate;
  venue: Venue;
  status: GameStatus;
}

interface GameDate {
  timezone: string;
  date: string;
  time: string;
  timestamp: number;
}

interface Venue {
  name: string;
  city: string;
}

interface GameStatus {
  short: string;
  long: string;
  timer: null | number;
}

interface League {
  id: number;
  name: string;
  season: string;
  logo: string;
  country: Country;
}

interface Country {
  name: string;
  code: string;
  flag: string;
}

interface Teams {
  home: Team;
  away: Team;
}

interface Team {
  id: number;
  name: string;
  logo: string;
}

interface Scores {
  home: ScoreDetails;
  away: ScoreDetails;
}

interface ScoreDetails {
  quarter_1: number | null;
  quarter_2: number | null;
  quarter_3: number | null;
  quarter_4: number | null;
  overtime: number | null;
  total: number;
}
