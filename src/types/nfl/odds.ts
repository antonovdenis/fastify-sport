interface Game {
  id: number;
}

interface League {
  id: number;
  name: string;
  season: number;
  logo: string;
}

interface Country {
  name: string;
  code: string;
  flag: string;
}

interface Value {
  value: string;
  odd: string;
}

interface Bet {
  id: number;
  name: string;
  values: Value[];
}

interface Bookmaker {
  id: number;
  name: string;
  bets: Bet[];
}

export interface NFLOddResponse {
  game: Game;
  league: League;
  country: Country;
  update: string;
  bookmakers: Bookmaker[];
}
