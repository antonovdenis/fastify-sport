import { join } from "path";
import AutoLoad, { AutoloadPluginOptions } from "@fastify/autoload";
import { FastifyPluginAsync, FastifyServerOptions } from "fastify";

export interface AppOptions
  extends FastifyServerOptions,
    Partial<AutoloadPluginOptions> {}

// Pass --options via CLI arguments in command to enable these options.
const options: AppOptions = {};

const app: FastifyPluginAsync<AppOptions> = async (
  fastify,
  opts
): Promise<void> => {
  // This loads all plugins defined in plugins
  // those should be support plugins that are reused
  // through your application
  void fastify.register(AutoLoad, {
    dir: join(__dirname, "plugins"),
    dirNameRoutePrefix: false,
    options: opts,
  });

  // This loads all plugins defined in routes
  // define your routes in one of these
  void fastify.register(AutoLoad, {
    dir: join(__dirname, "routes"),
    indexPattern: /.*routes\.ts/,
    ignorePattern: /.*\.ts/,
    autoHooksPattern: /.*hooks\.ts/,
    autoHooks: true,
    cascadeHooks: true,
    options: opts,
  });

  fastify.addHook("onReady", async () => {
    fastify.log.info(fastify.printRoutes({ commonPrefix: false }));
  });
};

export default app;
export { app, options };
