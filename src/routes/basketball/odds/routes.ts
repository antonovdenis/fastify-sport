import { FastifyPluginAsync, FastifyRequest } from "fastify";
import { ResponseItem } from "../../../types/basketball/odds";

const odds: FastifyPluginAsync = async (fastify, opts) => {
  const formatOdds = ({ game, bookmakers }: ResponseItem) => ({
    gameId: game.id,
    gameStatus: game.status.long,
    name: bookmakers[0].bets[0].name,
    bookmakers: bookmakers.map((bookmaker) => ({
      id: bookmaker.id,
      name: bookmaker.name,
      bets: bookmaker.bets[0].values,
    })),
  });

  fastify.get(
    "/:leagueId/:gameId",
    {
      schema: {
        params: {
          type: "object",
          properties: {
            gameId: {
              type: "number",
            },
            leagueId: {
              type: "number",
            },
          },
          required: ["leagueId", "gameId"],
        },
        querystring: {
          type: "object",
          properties: {
            betId: {
              type: "number",
            },
          },
        },
      },
    },
    async (
      request: FastifyRequest<{
        Params: { leagueId: number; gameId: number };
        Querystring: { betId: number };
      }>
    ) => {
      const { leagueId, gameId } = request.params;
      const { betId } = request.query;
      const data = (await fastify.basketball
        .request({
          // 3Way Result (Id: 1) - Moneyline
          // Home/Away (Id: 2) - Moneyline
          // Asian Handicap (Id: 3) - Line/Spread
          // Over/Under (Id: 4) - Over/Under points
          path: `/odds?season=2023-2024&bet=${betId !== undefined ? betId : 1}&league=${leagueId}&game=${gameId}`,
          method: "GET",
        })
        .then((res) => res.body.json())) as { response: ResponseItem[] };

      const odds = data.response.map(formatOdds);

      return { odds };
    }
  );
};

export default odds;
