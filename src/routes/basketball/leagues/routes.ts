import { FastifyPluginAsync, FastifyRequest } from "fastify";

const leagues: FastifyPluginAsync = async (fastify, opts): Promise<void> => {
  fastify.get(
    "/:countryCode?",
    {
      schema: {
        params: {
          type: "object",
          properties: {
            countryCode: {
              type: "string",
              minLength: 2,
              maxLength: 2,
            },
          },
        },
      },
    },
    async function (request: FastifyRequest<{ Params: { countryCode: string } }>) {
      const { countryCode } = request.params;
      const data = (await fastify.basketball
        .request({
          path: `/leagues${countryCode !== undefined ? "?code=" + countryCode : ""}`,
          method: "GET",
        })
        .then((res) => res.body.json())) as {
        response: {
          id: number;
          name: string;
          country: { name: string };
        }[];
      };

      const leagues = data.response.map(({ id, name, country }) => ({
        id: id,
        name: name,
        country: country.name,
      }));

      return { leagues };
    }
  );
};

export default leagues;
