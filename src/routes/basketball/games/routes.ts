import { FastifyPluginAsync, FastifyRequest } from "fastify";
import { BasketballGame } from "../../../types/basketball/games";

// "id": 12,
// "name": "NBA"

const games: FastifyPluginAsync = async (fastify, opts): Promise<void> => {
  fastify.get(
    "/:leagueId",
    {
      schema: {
        params: {
          type: "object",
          properties: {
            leagueId: {
              type: "number",
            },
          },
          required: ["leagueId"],
        },
      },
    },
    async (request: FastifyRequest<{ Params: { leagueId: number } }>) => {
      const formatGames = ({ id, date, status, teams, scores }: BasketballGame) => ({
        id,
        date,
        status: status.long,
        home: { ...teams.home, pointsScored: scores.home.total },
        away: { ...teams.away, pointsScored: scores.away.total },
      });

      const { leagueId } = request.params;
      const data = (await fastify.basketball
        .request({
          // &date=${new Date().toLocaleDateString("en-CA", {
          //     year: "numeric",
          //     month: "2-digit",
          //     day: "2-digit",
          //   })}
          path: `/games?season=2023-2024&league=${leagueId}&date=2023-11-14`,
          method: "GET",
        })
        .then((res) => res.body.json())) as { response: BasketballGame[] };

      const games = data.response.map(formatGames);

      return { games };
    }
  );
};

export default games;
