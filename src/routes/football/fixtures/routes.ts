import { FastifyPluginAsync, FastifyRequest } from "fastify";
import { MatchResponse } from "../../../types/football/fixtures";

const fixtures: FastifyPluginAsync = async (fastify, opts): Promise<void> => {
  const formatFix = ({ fixture, teams }: MatchResponse) => ({
    id: fixture.id,
    date: fixture.date,
    status: fixture.status.long,
    homeTeam: teams.home,
    awayTeam: teams.away,
  });

  fastify.get(
    "/:leagueId",
    {
      schema: {
        params: {
          type: "object",
          properties: {
            leagueId: {
              type: "number",
            },
          },
          required: ["leagueId"],
        },
      },
    },
    async function (request: FastifyRequest<{ Params: { leagueId: number } }>) {
      const { leagueId } = request.params;
      // If you don't provide specifics (league, season) about the fixtures then data is not returned
      const data = (await fastify.football
        .request({
          // fixtures that have not started yet change status to see others
          // https://www.api-football.com/documentation-v3#tag/Fixtures/operation/get-fixtures
          path: `/fixtures?season=2023&status=NS&league=${leagueId}`,
          method: "GET",
        })
        .then((res) => res.body.json())) as { response: MatchResponse[] };

      const matches = data.response.map(formatFix);

      return { matches };
    }
  );
};

export default fixtures;
