import { FastifyPluginAsync, FastifyRequest } from "fastify";

const leagues: FastifyPluginAsync = async (fastify, opts): Promise<void> => {
  fastify.get(
    "/:countryCode?",
    {
      schema: {
        params: {
          type: "object",
          properties: {
            countryCode: {
              type: "string",
              minLength: 2,
              maxLength: 2,
            },
          },
        },
      },
    },
    async function (request: FastifyRequest<{ Params: { countryCode: string } }>) {
      const { countryCode } = request.params;
      const data = (await fastify.football
        .request({
          path: `/leagues${countryCode !== undefined ? "?code=" + countryCode : ""}`,
          method: "GET",
        })
        .then((res) => res.body.json())) as {
        response: {
          country: { name: string };
          league: { id: number; name: string };
        }[];
      };

      const leagues = data.response.map(({ league, country }) => ({
        id: league.id,
        name: league.name,
        country: country.name,
      }));

      return { leagues };
    }
  );
};

export default leagues;
