import { FastifyPluginAsync, FastifyRequest } from "fastify";
import { ResponseOdds } from "../../../types/football/odds";

const odds: FastifyPluginAsync = async (fastify, opts) => {
  const formatOdd = ({ fixture, bookmakers }: ResponseOdds) => ({
    id: fixture.id,
    name: bookmakers[0].bets[0].name,
    bookmakers: bookmakers.map((bookmaker) => ({
      id: bookmaker.id,
      name: bookmaker.name,
      bets: bookmaker.bets[0].values,
    })),
  });

  fastify.get(
    "/:fixtureId",
    {
      schema: {
        params: {
          type: "object",
          properties: {
            fixtureId: {
              type: "number",
            },
          },
          required: ["fixtureId"],
        },
        querystring: {
          type: "object",
          properties: {
            betId: {
              type: "number",
            },
          },
        },
      },
    },
    async (
      request: FastifyRequest<{
        Params: { fixtureId: number };
        Querystring: { betId: number };
      }>
    ) => {
      const { fixtureId } = request.params;
      const { betId } = request.query;
      const data = (await fastify.football
        .request({
          // "id": 1 "name": "Match Winner"
          // "id": 5 "name": "Goals Over/Under"
          // "id": 118 "name": "Home Win/Over"
          // "id": 119 "name": "Home Win/Under"
          // "id": 120 "name": "Away Win/Over"
          // "id": 121 "name": "Away Win/Under"
          path: `/odds?season=2023&bet=${betId !== undefined ? betId : 1}&fixture=${fixtureId}`,
          method: "GET",
        })
        .then((res) => res.body.json())) as { response: ResponseOdds[] };

      const odds = data.response.map(formatOdd);

      return { odds: odds[0] };
    }
  );
};

export default odds;
