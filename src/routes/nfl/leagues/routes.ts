import { FastifyPluginAsync } from "fastify";

const leagues: FastifyPluginAsync = async (fastify, opts) => {
  fastify.get("/", async () => {
    const data = (await fastify.nfl
      .request({
        path: "/leagues?season=2023",
        method: "GET",
      })
      .then((res) => res.body.json())) as { response: unknown[] };
    return { leagues: data.response };
  });
};

export default leagues;
