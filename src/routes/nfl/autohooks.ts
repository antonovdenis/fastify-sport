import fp from "fastify-plugin";
import { Dispatcher, Pool } from "undici";

export default fp(
  async (fastify, opts) => {
    const pool = new Pool("https://v1.american-football.api-sports.io", {
      connections: 10,
      allowH2: true,
    });

    fastify.decorate("nfl", {
      request: (options: Dispatcher.RequestOptions) => {
        const defaultHeaders = {
          "x-apisports-key": fastify.secrets.SPORT_API_KEY,
        };
        options.headers = { ...defaultHeaders, ...options.headers };
        return pool.request(options);
      },
    });
  },
  {
    name: "nfl-api",
  }
);

declare module "fastify" {
  export interface FastifyInstance {
    nfl: {
      request: (options: Dispatcher.RequestOptions) => Promise<Dispatcher.ResponseData>;
    };
  }
}
