import { FastifyPluginAsync } from "fastify";
import { NFLGameDetails } from "../../../types/nfl/games";

const games: FastifyPluginAsync = async (fastify, opts) => {
  const formatGame = ({ game, teams, scores }: NFLGameDetails) => ({
    gameId: game.id,
    date: game.date.date,
    status: game.status.long,
    homeTeam: { ...teams.home, scores: scores.home.total },
    awayTeam: { ...teams.away, scores: scores.away.total },
  });

  fastify.get("/", async () => {
    const data = (await fastify.nfl
      .request({
        path: "/games?league=1&season=2023",
        method: "GET",
      })
      .then((res) => res.body.json())) as { response: NFLGameDetails[] };

    const games = data.response.map(formatGame);

    return { games };
  });
};

export default games;
