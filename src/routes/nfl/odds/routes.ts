import { FastifyPluginAsync, FastifyRequest } from "fastify";
import { NFLOddResponse } from "../../../types/nfl/odds";

const odds: FastifyPluginAsync = async (fastify, opts) => {
  fastify.get(
    "/:gameId",
    {
      schema: {
        params: {
          type: "object",
          required: ["gameId"],
          properties: {
            gameId: {
              type: "number",
            },
          },
        },
        querystring: {
          type: "object",
          properties: {
            betId: {
              type: "number",
            },
          },
        },
      },
    },
    async (request: FastifyRequest<{ Params: { gameId: number }; Querystring: { betId: number } }>) => {
      const formatOdds = ({ game, bookmakers }: NFLOddResponse) => ({
        gameId: game.id,
        name: bookmakers[0].bets[0].name,
        bookmakers: bookmakers.map((bookmaker) => ({
          id: bookmaker.id,
          name: bookmaker.name,
          bets: bookmaker.bets[0].values,
        })),
      });

      const { gameId } = request.params;
      const { betId } = request.query;
      const data = (await fastify.nfl
        // Home/Away (Id: 1) - Moneyline
        // 3Way Result (Id: 55) - Moneyline
        // Asian Handicap (Id: 2) - Line/Spread
        // Over/Under (Id: 3) - Total points
        // Total - Home (Id: 8) and Total - Away (Id: 9)
        .request({
          path: `/odds?game=${gameId}&bet=${betId !== undefined ? betId : 1}`,
          method: "GET",
        })
        .then((res) => res.body.json())) as { response: NFLOddResponse[] };

      const odds = data.response.map(formatOdds);

      return { odds };
    }
  );
};

export default odds;
