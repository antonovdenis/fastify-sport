import { FastifyPluginAsync, FastifyRequest } from "fastify";
import { VolleyballMatch } from "../../../types/volleyball/games";

const games: FastifyPluginAsync = async (fastify, opts) => {
  fastify.get(
    "/:leagueId",
    {
      schema: {
        params: {
          type: "object",
          required: ["leagueId"],
          properties: {
            leagueId: {
              type: "number",
            },
          },
        },
      },
    },
    async (request: FastifyRequest<{ Params: { leagueId: number } }>) => {
      const formatGame = ({ id, date, status, teams, scores }: VolleyballMatch) => ({
        id,
        date,
        status: status.long,
        homeTeam: { ...teams.home, score: scores.home },
        awayTeam: { ...teams.away, score: scores.away },
      });

      const { leagueId } = request.params;
      const data = (await fastify.volleyball
        .request({
          path: `/games?season=2023&league=${leagueId}`,
          method: "GET",
        })
        .then((res) => res.body.json())) as { response: VolleyballMatch[] };

      const games = data.response.map(formatGame);
      return { games };
    }
  );
};

export default games;
