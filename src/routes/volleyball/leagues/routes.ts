import { FastifyPluginAsync, FastifyRequest } from "fastify";

const leagues: FastifyPluginAsync = async (fastify, opts) => {
  fastify.get(
    "/:country",
    {
      schema: {
        params: {
          type: "object",
          required: ["country"],
          properties: {
            country: {
              type: "string",
            },
          },
        },
      },
    },
    async (request: FastifyRequest<{ Params: { country: string } }>) => {
      const { country } = request.params;
      const data = (await fastify.volleyball
        .request({
          path: `/leagues?season=2023&country=${country}`,
          method: "GET",
        })
        .then((res) => res.body.json())) as { response: { id: string; name: string; country: { name: string } }[] };

      const leagues = data.response.map(({ id, name, country }) => ({
        id,
        name,
        country: country.name,
      }));

      return { leagues };
    }
  );
};

export default leagues;
