import fp from "fastify-plugin";
import { Dispatcher, Pool } from "undici";

export default fp(
  async (fastify, opts) => {
    const pool = new Pool("https://v1.volleyball.api-sports.io", {
      connections: 10,
      allowH2: true,
    });

    fastify.decorate("volleyball", {
      request: (options: Dispatcher.RequestOptions) => {
        const defaultHeaders = {
          "x-apisports-key": fastify.secrets.SPORT_API_KEY,
        };
        options.headers = { ...defaultHeaders, ...options.headers };
        return pool.request(options);
      },
    });
  },
  {
    name: "volleyball-api",
  }
);

declare module "fastify" {
  export interface FastifyInstance {
    volleyball: {
      request: (options: Dispatcher.RequestOptions) => Promise<Dispatcher.ResponseData>;
    };
  }
}
