import { FastifyPluginAsync, FastifyRequest } from "fastify";
import { GameBettingInfo } from "../../../types/volleyball/odds";

const odds: FastifyPluginAsync = async (fastify, opts) => {
  fastify.get(
    "/:gameId",
    {
      schema: {
        params: {
          type: "object",
          required: ["gameId"],
          properties: {
            gameId: {
              type: "number",
            },
          },
        },
        querystring: {
          type: "object",
          properties: {
            betId: {
              type: "number",
            },
          },
        },
      },
    },
    async (request: FastifyRequest<{ Params: { gameId: number }; Querystring: { betId: number } }>) => {
      const formatOdd = ({ game, bookmakers }: GameBettingInfo) => ({
        gameId: game.id,
        name: bookmakers[0].bets[0].name,
        bookmakers: bookmakers.map((bookmaker) => ({
          id: bookmaker.id,
          name: bookmaker.name,
          bets: bookmaker.bets[0].values,
        })),
      });

      const { gameId } = request.params;
      const { betId } = request.query;
      const data = (await fastify.volleyball
        // Home/Away (Id: 1):
        // Asian Handicap (Sets) (Id: 13)
        // Asian Handicap (Games) (Id: 14)
        // Over/Under (Id: 2)
        // Over/Under by Games in Match (Id: 8)
        .request({
          path: `/odds?bet=${betId !== undefined ? betId : 1}&game=${gameId}`,
          method: "GET",
        })
        .then((res) => res.body.json())) as { response: GameBettingInfo[] };

      const odds = data.response.map(formatOdd);

      return { odds };
    }
  );
};

export default odds;
